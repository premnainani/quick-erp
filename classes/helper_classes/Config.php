<?php

class config
{

    protected $config;
    public function __construct()
    {
        $this->config = parse_ini_file(__DIR__."/../../config.ini");
    }

    public function get(string $key):string
    {
        if(isset($this->config[$key]))
        {
            return $this->config[$key];
        }
        /**
         * If the config not found then throw error message depending on debug value of config file i.e throw error depending on what debug says(throw error , partially or not!)
         * TODO: Convert the below die() to appropriate error message
         */

         die('This config cannot be found : '.$key);
    }
}