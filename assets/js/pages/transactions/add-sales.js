var id = 2;
var baseURL = window.location.origin;
var filePath = "/helper/routing.php";

function deleteProduct(delete_id) {
    var elements = document.getElementsByClassName("product_row");
    if(elements.length != 1) {
        $("#element_"+delete_id).remove();
    }
}
function addProduct() {
    $("#products_container").append(
        `
        <!-- BEGIN: PRODUCT CUSTOM CONTROL -->
        <div class="row product_row" id="element_${id}">
            <!-- BEGIN: CATEGORY SELECT -->
            <div class="col-md-2">
                <div class="form-group">
                    <label for="">Category</label>
                    <select id="category_${id}" class="form-control category_select">
                        <option disabled selected>Select Category</option>
                    </select>
                </div>
            </div>
            <!-- END: CATEGORY SELECT -->
            <!-- BEGIN: PRODUCTS SELECT -->
            <div class="col-md-3">
                <div class="form-group">
                    <label for="">Products</label>
                    <select name="product_id[]" id="product_${id}"
                            class="form-control">
                        <option disabled selected>Select Product</option>
                    </select>
                </div>
            </div>
            <!-- END: PRODUCTS SELECT -->
            <!-- BEGIN: SELLING PRICE -->
            <div class="col-md-2">
                <div class="form-group">
                    <label for="">Selling Price</label>
                    <input type="text" id="selling_price_${id}"
                        class="form-control" disabled>
                </div>
            </div>
            <!-- END: SELLING PRICE -->
            <!-- BEGIN: QUANTITY -->
            <div class="col-md-1">
                <div class="form-group">
                    <label for="">Quantity</label>
                    <input type="number" name="quantity[]" id="quantity_${id}"
                    class="form-control"
                    value="0"> 
                </div>
            </div>
            <!-- END: QUANTITY -->
            <!-- BEGIN: DISCOUNT -->
            <div class="col-md-1">
                <div class="form-group">
                    <label for="">Discount</label>
                    <input type="number" name="discount[]" id="discount_${id}"
                        class="form-control"
                        value="0">
                </div>
            </div>
            <!-- END: DISCOUNT -->
            <!-- BEGIN: FINAL RATE -->
            <div class="col-md-2">
                <div class="form-group">
                    <label for="">Final Rate</label>
                    <input type="number" name="final_rate[]" id="final_rate_${id}"
                    class="form-control"
                    value="0" readonly>
                </div>
            </div>
            <!-- END: FINAL RATE -->
            <!-- BEGIN: DELETE BUTTON -->
            <div class="col-md-1">
                <button onclick="deleteProduct(${id})"
                        type="button"
                        class="btn btn-danger"
                        style="margin-top: 40%;">
                    <i class="far fa-trash-alt"></i>
                </button>
            </div>
            <!-- END: DELETE BUTTON -->
        </div>
        <!-- END: PRODUCT CUSTOM CONTROL -->
        `
    );
    $.ajax({
        url: baseURL+filePath,
        method: 'POST',
        data: {
            getCategories: true
        },
        dataType: 'json',
        success: function(categories) {
            categories.forEach(function (category) {
                $("#category_"+id).append(
                    `<option value='${category.id}'>${category.name}</option>`
                );
            });
            id++;
        }
    });

}

$("#products_container").on('change', '.category_select', function () {
    var element_id = $(this).attr('id').split("_")[1];
    var category_id = this.value; 
    $.ajax({        
        url: baseURL+filePath,
        method: 'POST',
        data: {
            getProductsByCategoryID: true,
            categoryID: category_id
        },
        dataType: 'json',
        success: function(products) {
            $("#product_"+element_id).empty();
            $("#product_"+element_id).append("<option disabled selected>Select Product</option>");
            products.forEach(function (product) {
                $("#product_"+element_id).append(
                    `<option value='${product.id}'>${product.name}</option>`
                );
            });
        
        }
    })
});